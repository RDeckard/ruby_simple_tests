# frozen_string_literal: true

nb_of_threads = 10

threads = []
now = Time.now

nb_of_threads.times do
  threads << Thread.new do
    sleep 1
    rand(1..6)
  end
end

puts threads.map(&:value).sum.to_f / nb_of_threads

puts "rolls: #{nb_of_threads}"
puts "time: #{Time.now - now}"
