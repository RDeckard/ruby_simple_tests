#!/usr/bin/env ruby
# frozen_string_literal: true

class Game
  TEMPTATION  = 3
  REWARD      = 2
  PUNISHMENT  = 0
  RISK        = -1

  def initialize(nb_of_turns, players)
    @nb_of_turns = nb_of_turns
    @turns = []
    @players = players
  end

  def run
    raise 'a game can be runned only once' if @turns.any?

    @nb_of_turns.times do
      @turns << run_turn
    end

    results
  end

  def results
    raise 'run the game first !' if @turns.empty?

    @results ||=
      @players.to_h do |player|
        [player, @turns.map { |turn| turn[player][:result] }.sum]
      end
  end

  private

  def run_turn
    turn_decisions = @players.map { |player| player.decide(@turns) }
    turn_results   = case turn_decisions
                     when %w[cooperate cooperate]
                       [REWARD, REWARD]
                     when %w[cooperate defect]
                       [RISK, TEMPTATION]
                     when %w[defect cooperate]
                       [TEMPTATION, RISK]
                     when %w[defect defect]
                       [PUNISHMENT, PUNISHMENT]
                     end

    [0, 1].to_h do |index|
      [
        @players[index],
        { result: turn_results[index], decision: turn_decisions[index] }
      ]
    end
  end
end

class Tournament
  attr_reader :nb_of_events, :turns_per_game, :verbosity, :player_types

  def initialize(nb_of_events = 1, turns_per_game = 1, verbosity = false)
    @nb_of_events   = nb_of_events
    @turns_per_game = turns_per_game
    @verbosity      = verbosity
    @games_results  = []
    @player_types   = Player.descendants
  end

  def run
    raise 'a tournament can be runned only once' if @games_results.any?

    nb_of_events.times do
      run_event
    end

    rounds_per_player = nb_of_events * player_types.count * turns_per_game

    puts <<~TEXT
      ---------------
      Global results:
      ---------------
      Events:  #{big_int_format(nb_of_events)}
      Turns:   #{big_int_format(turns_per_game)}/#{big_int_format(turns_per_game * nb_of_events)}
      Players: #{big_int_format(player_types.count)}
      Turns:   #{big_int_format(rounds_per_player)}/#{big_int_format(rounds_per_player * player_types.count)}
      ---
    TEXT

    results.each do |player_type, score|
      printf "%-25s : %10s ( average: %10s )\n",
             player_type,
             big_int_format(score),
             (score.to_f / rounds_per_player).round(2)
    end

    total = results.values.sum
    puts "#{'-' * 62}"
    printf "%-25s : %10s ( average: %10s )\n",
           'TOTAL',
           big_int_format(total),
           (total.to_f / (rounds_per_player * player_types.count)).round(2)


    puts
    puts <<~TEXT
      -------
      POINTS:
      -------
    TEXT
    puts "---\nPOINTS:"
    Game.constants.each do |constant_name|
      printf "%-10s = %5s\n",
             constant_name,
             Module.const_get("Game::#{constant_name}")
    end
  end

  def results
    raise 'run the tournament first !' if @games_results.empty?

    @results ||=
      player_types
      .map do |player_type|
        [
          player_type,
          @games_results.map { |game_results| game_results.map { |player, value| value if player.instance_of?(player_type) }.compact.sum }.sum
        ]
      end
      .sort_by { |game_results| game_results[1] }.reverse.to_h
  end

  private

  def run_event
    player_types.each_with_index do |first_player_type, index|
      (index..(player_types.count - 1)).each do |i|
        second_player_type = player_types[i]
        @games_results << Game.new(turns_per_game, [first_player_type.new, second_player_type.new]).run.tap do |game|
          first_player_type   = game.to_a.first.first.class
          first_player_score  = game.to_a.first.last
          second_player_type  = game.to_a.last.first.class
          second_player_score = game.to_a.last.last
          if verbosity
            printf "%25s  VS  %-25s\n", first_player_type, second_player_type
            printf "%25s      %-25s\n", first_player_score, second_player_score
          end
        end
      end
    end
  end

  def big_int_format(int)
    int.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\1,').reverse
  end
end

class Player
  def self.descendants
    ObjectSpace.each_object(singleton_class).select { |k| k < self }
  end
end

class RandomPlayer < Player
  def decide(_turns)
    rand(2) == 0 ? 'cooperate' : 'defect'
  end
end

class NiceRandomPlayer < Player
  def decide(_turns)
    rand(3) == 0 ? 'defect' : 'cooperate'
  end
end

class BadRandomPlayer < Player
  def decide(_turns)
    rand(3) == 0 ? 'cooperate' : 'defect'
  end
end

class ReallyNiceRandomPlayer < Player
  def decide(_turns)
    rand(10) == 0 ? 'defect' : 'cooperate'
  end
end

class ReallyBadRandomPlayer < Player
  def decide(_turns)
    rand(10) == 0 ? 'cooperate' : 'defect'
  end
end

class NaivePlayer < Player
  def decide(_turns)
    'cooperate'
  end
end

class MeanPlayer < Player
  def decide(_turns)
    'defect'
  end
end

class NiceRancorousPlayer < Player
  def decide(turns)
    return 'cooperate' if turns.empty?
    return 'defect' if @rancorous_on

    if turns.last.reject { |player, _value| player == self }.values.first[:decision] == 'defect'
      @rancorous_on = true
      'defect'
    else
      'cooperate'
    end
  end
end

class BadRancorousPlayer < Player
  def decide(turns)
    return 'defect' if turns.empty?
    return 'defect' if @rancorous_on

    if turns.last.reject { |player, _value| player == self }.values.first[:decision] == 'defect'
      @rancorous_on = true
      'defect'
    else
      'cooperate'
    end
  end
end

class AdvTitForTatPlayer < Player
  def decide(turns)
    if turns.last(2).map { |turn| turn.reject { |player, _value| player == self }.values.first[:decision] }.include? 'defect'
      'defect'
    else
      'cooperate'
    end
  end
end

class TitForTatPlayer < Player
  def decide(turns)
    if turns.last(1).map { |turn| turn.reject { |player, _value| player == self }.values.first[:decision] }.include? 'defect'
      'defect'
    else
      'cooperate'
    end
  end
end

class DetectivePlayer < Player
  def decide(turns)
    case turns.count
    when 0 then 'cooperate'
    when 1 then 'defect'
    when 2 then 'cooperate'
    when 3 then 'cooperate'
    else
      @mode ||=
        if turns.map { |turn| turn.reject { |player, _value| player == self }.values.first[:decision] }.include? 'defect'
          :tit_for_tat
        else
          :mean
        end
      case @mode
      when :tit_for_tat
        if turns.last(1).map { |turn| turn.reject { |player, _value| player == self }.values.first[:decision] }.include? 'defect'
          'defect'
        else
          'cooperate'
        end
      when :mean
        'defect'
      end
    end
  end
end

# Example
Tournament.new(
  (ARGV[0] || '10').to_i,
  (ARGV[1] || '100').to_i,
  (ARGV[2] == 'verbose')
).run
