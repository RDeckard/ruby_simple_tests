# frozen_string_literal: true

puts 'start programme'
th1 = Thread.new do
  puts '-- start th1'
  (1..3).each { |i| puts "-- th1 : #{i}"; sleep 1 }
  puts '-- end th1'
end

th2 = Thread.new do
  puts '-- start th2'
  (1..3).each { |i| puts "-- th2 : #{i}"; sleep 1 }
  puts '-- end th2'
end

th3 = Thread.new do
  puts '-- start th3'
  (1..3).each { |i| puts "-- th3 : #{i}"; sleep 1 }
  puts '-- end th3'
end

puts '1'

th1.join

puts '2'

th2.join

puts '3'

th3.join

puts '4'
