# frozen_string_literal: true

module ManageInstances
  def self.prepended(base)
    class << base
      attr_accessor :instances

      def all
        instances.dup
      end

      def all_with_index(name_only: false)
        all.each_with_index.map do |instance, index|
          [
            index,
            (name_only ? instance.name : instance)
          ]
        end
      end

      def [](index)
        instances[index]
      end

      def count
        instances.count
      end
    end
  end

  def initialize(*args)
    super
    self.class.instances ||= []
    self.class.instances << self
  end

  def destroy
    self.class.instances.delete(self)
  end
end

class Task
  prepend ManageInstances # Only this line is required !
  attr_accessor :name, :cost, :children

  def initialize(name, cost = nil)
    @name = name
    @cost = cost
    @children = []
  end

  def total_cost
    cost.to_i + children.map(&:total_cost).sum
  end

  def to_s
    string = +"#{name} (cost: #{cost || 'none'}:#{total_cost})"
    if children.any?
      string << ": [#{children.map(&:to_s).join('; ').gsub(/\\\"/, '')}]"
    end
    string
  end
end

# Example
basic_task1 = Task.new('Basic task 1', 5)
basic_task2 = Task.new('Basic task 2', 4)

manager_task1 = Task.new('Complex task 1', 2)
manager_task1.children = [basic_task1, basic_task2]
manager_task2 = Task.new('Complex task 2', 3)

main_task = Task.new('Global task')
main_task.children = [manager_task1, manager_task2]

puts '--- Examples of Task usage ---'
puts basic_task2
puts manager_task1
puts main_task

puts "\n--- Examples of ManageInstances usage (when prepended to Task) ---"
puts '-- Task.all'
p Task.all
puts '-- Task.all_with_index'
p Task.all_with_index
puts '-- Task.all_with_index(name_only: true)'
p Task.all_with_index(name_only: true)
