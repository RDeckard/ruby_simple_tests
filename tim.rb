# frozen_string_literal: true

class Tim
  attr_reader :screen, :coords

  def initialize(screen, x = 4, y = 4)
    @screen = screen
    @coords = Struct.new(:x, :y).new(x, y)
  end

  def go_up
    @coords.y -= 1 unless coords.y == 0
  end

  def go_down
    @coords.y += 1 unless coords.y == screen.map.size - 1
  end

  def go_left
    @coords.x -= 1 unless coords.x == 0
  end

  def go_right
    @coords.x += 1 unless coords.x == screen.map[coords.y].size - 1
  end
end

class Screen
  attr_reader :map
  attr_accessor :tim

  def initialize
    @map = Array.new(9) { Array.new(9, '.') }
  end

  def wander(steps = 100)
    steps.times do
      sleep 0.01
      tim.send %i[go_up go_down go_left go_right].sample
      system('clear') || system('cls')
      draw
    end
  end

  def draw
    draw = Marshal.load(Marshal.dump(map))
    draw[tim.coords.x][tim.coords.y] = '@'
    draw.each do |row|
      puts row.join(' ')
    end
  end
end

screen = Screen.new
screen.tim = Tim.new(screen)

screen.wander
