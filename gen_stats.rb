#!/usr/bin/env ruby
# frozen_string_literal: true

class GenStats
  attr_reader :filename, :regex, :range, :extracted_values, :stats, :csv

  def initialize(filename, regex = '', range = 1)
    @filename = filename
    @regex = regex
    @range = range
  end

  def call
    @extracted_values ||=
      File.readlines(filename)
          .map { |row| row[Regexp.new("(?<=#{regex.strip} )[0-9]+$")]&.to_i }
          .compact
          .sort
    @stats ||=
      extracted_values
        .each_with_object(Hash.new(0)) { |value, acc| acc[value / range * range] += 1 }
  end

  def gen_stats_file(name = "stats-#{regex.strip.gsub(' ', '-')}-range-#{range}.csv")
    @csv ||=
      stats
        .map { |k, v| (range == 1 ? k.to_s : "#{k}-#{k + range - 1}") + ",#{v}\n" }
        .join
    File.write(name, csv)
  end
end

# PROGRAM
filename = ARGV[0]
regex    = ARGV[1]       || ''
range    = ARGV[2]&.to_i || 1

unless filename
  puts 'Please supply a filename'
  return
end

gen_stats = GenStats.new(filename, regex, range)
gen_stats.call
p gen_stats.extracted_values
p gen_stats.stats
gen_stats.gen_stats_file
p gen_stats.csv
