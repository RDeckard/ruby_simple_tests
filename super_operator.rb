# frozen_string_literal: true

class Op
  attr_reader :operator_level, :origin, :level_to_breakdown

  def initialize(operator_level, origin = nil, level_to_breakdown: nil)
    @operator_level     = operator_level.abs.to_i
    @origin             = origin || @operator_level
    @level_to_breakdown = (level_to_breakdown || 4).abs.to_i
  end

  def apply(first_operand, last_operand = nil)
    last_operand ||= first_operand
    if operator_level == 0
      # last_operand = first_operand
      puts "#{operator_level == origin ? '' : 'So '}#{last_operand} #{print_op} (op lvl #{operator_level})"
    else
      puts "#{operator_level == origin ? '' : 'So '}#{first_operand} #{print_op} #{last_operand} (op lvl #{operator_level})"
    end
    res = if (operator_level == 0) && (level_to_breakdown >= 0)
            last_operand + 1
          elsif (operator_level == 1) && (level_to_breakdown >= 1)
            first_operand + last_operand
          elsif (operator_level == 2) && (level_to_breakdown >= 2)
            first_operand * last_operand
          elsif (operator_level == 3) && (level_to_breakdown >= 3)
            first_operand**last_operand
          else
            hyperop(first_operand, last_operand)
          end
    puts "= #{res}"
    res
  end

  def print_op
    case operator_level
    when 0 then 'INCREMENTATION'
    when 1 then '+'
    when 2 then '*'
    else        '↑' * (operator_level - 2)
    end
  end

  private

  def hyperop(first_operand, last_operand)
    subop = Op.new(operator_level - 1, origin, level_to_breakdown: level_to_breakdown)
    last_operand += 1 if subop.operator_level == 0

    puts "which is like calling recursively #{last_operand - 1} time#{'s' if last_operand > 2} '#{subop.print_op}' on #{first_operand}"
    last_operand.times.map { first_operand } # create an array with 'last_operand' occurences of 'first_operand' within.
     .reduce do |x, y| # apply the given 'op' operation between each
      subop.apply(y, x)
    end
  end

  def puts(text)
    super "#{'  ' * (origin - operator_level)}#{text}"
  end
end

# Example
Op.new(4, level_to_breakdown: 2).apply(3, 3)
