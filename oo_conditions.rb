# frozen_string_literal: true

class Object
  def if_truthy(then_do:, else_do: nil)
    then_do.call
  end

  def if_falsy(then_do:, else_do: -> {})
    else_do.call
  end
end

module Falsy
  def if_truthy(then_do:, else_do: -> {})
    else_do.call
  end

  def if_falsy(then_do:, else_do: nil)
    then_do.call
  end
end

FalseClass.include Falsy
NilClass.include   Falsy

TESTS = {
  if_truthy: {
    on_truthy_object: { param_to_execute: :then_do },
    on_falsy_object: { param_to_execute: :else_do }
  },
  if_falsy: {
    on_truthy_object: { param_to_execute: :else_do },
    on_falsy_object: { param_to_execute: :then_do }
  }
}.freeze
SUBJECTS = [Object.new, nil, false].freeze

TESTS.each do |tested_method, infos|
  describe tested_method, 'method' do
    SUBJECTS.each do |subject|
      context "`#{subject.class}` instance" do
        it do
          is_expected.to respond_to(tested_method).with_keywords(:then_do)
        end

        it do
          is_expected.to respond_to(tested_method).with_keywords(:then_do, :else_do)
        end

        param_to_execute =
          infos[subject ? :on_truthy_object : :on_falsy_object][:param_to_execute]
        it "should execute its :#{param_to_execute} parameter" do
          x = 1
          subject.send tested_method, then_do: -> { x += 1 },
                                      else_do: -> { x -= 1 }
          expect(x).to eq(
            case param_to_execute
            when :then_do then 2
            when :else_do then 0
            end
          )
        end

        if param_to_execute == :else_do
          it 'should do nothing when no :else_do parameter was given' do
            x = 1
            subject.send tested_method, then_do: -> { x += 1 }
            expect(x).to eq 1
          end
        end
      end
    end
  end
end
